Clone me with `git clone git@gitlab.com:Yonder-Dynamics/catkin_ws.git --recursive`!

This repo contains all the sub-packages we use to run the rover. These include website, controls, and more.

## Setup
Run the `setup.sh`.

## Making Changes
After you `cd` into `src/<pkg>`, you can make changes.

After you are done, since all the sub-packages are stored as a submodule, you have to push twice. In particular:
1. commit and push inside the submodule dir
2. commit and push inside `catkin_ws` (if needed)

Some useful git commands:
1. `git pull --recurse-submodules` to download and merge the remote change.
2. `git checkout <branch>` to switch to a branch.
3. `git checkout -b <branch>` to create and switch to a branch.
4. `git add <file/dir>` to stage new changes.
5. `git commit <-m commit_message>`. To write the commit message with an editor.
6. `git push` to upload local change
