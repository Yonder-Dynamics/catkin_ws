#!/usr/bin/zsh

echo "Setting up..."

#echo "Installing oh my zsh..."
#sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "Modifying ~/.zshrc..."
echo 'source /opt/ros/noetic/setup.zsh' >> $HOME/.zshrc
echo 'source ~/catkin_ws/devel/setup.zsh' >> $HOME/.zshrc

echo "Sourcing ~/.zshrc"
source $HOME/.zshrc

echo "Updating Rosdep..."
rosdep update

echo "Installing deps with Rosdep..."
rosdep install --from-paths src --ignore-src -r -y

echo "Installing node..."
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install nodejs

echo "Installing frontend deps..."
cd src/frontend_interaction
npm install

echo "Building the website..."
npm run build

echo "Installing backend deps..."
cd -
cd src/backend_interaction
pip3 install -r requirements.txt

echo "Getting build tools..."
cd -
sudo pip3 install git+https://github.com/catkin/catkin_tools.git

echo "Building..."
catkin build

echo "Done."
